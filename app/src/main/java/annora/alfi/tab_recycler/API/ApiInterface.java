package annora.alfi.tab_recycler.API;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("latest.php")
    Call<Items> getData();

    @GET("categories.php")
    Call<Items2> getData2();


}
