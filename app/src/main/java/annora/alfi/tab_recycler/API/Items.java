package annora.alfi.tab_recycler.API;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Items {

    @SerializedName("meals")
    private ArrayList<mealList> meals;

    public ArrayList<mealList> getMeals() {
        return meals;
    }

    public class mealList {
        @SerializedName("strMeal")
        private String strMeal;
        @SerializedName("strCategory")
        private String strCategory;
        @SerializedName("strMealThumb")
        private String strMT;

        public String getStrMeal() {
            return strMeal;
        }

        public String getStrCategory() {
            return strCategory;
        }

        public String getStrMT() {
            return strMT;
        }
    }


}
