package annora.alfi.tab_recycler.API;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Items2 {

    @SerializedName("categories")
    private ArrayList<catPict> catPicts;

    public ArrayList<catPict> getCatPicts() {
        return catPicts;
    }

    public class catPict {
        @SerializedName("strCategoryThumb")
        private String sct;

        public String getSct() {
            return sct;
        }
    }
}
