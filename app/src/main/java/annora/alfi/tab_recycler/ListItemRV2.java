package annora.alfi.tab_recycler;

public class ListItemRV2 {

    private int image;
    private String judul;
    private String desc;

    public ListItemRV2(int image, String judul, String desc) {
        this.image = image;
        this.judul = judul;
        this.desc = desc;
    }

    public int getImage() {
        return image;
    }

    public String getJudul() {
        return judul;
    }

    public String getDesc() {
        return desc;
    }
}
