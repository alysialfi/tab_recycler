package annora.alfi.tab_recycler;

public class ListItemRV2_2 {
    private int imageView;
    private String textView1;
    private String textView2;

    public ListItemRV2_2(int imageView, String textView1, String textView2) {
        this.imageView = imageView;
        this.textView1 = textView1;
        this.textView2 = textView2;
    }

    public int getImageView() {
        return imageView;
    }

    public String getTextView1() {
        return textView1;
    }

    public String getTextView2() {
        return textView2;
    }
}

