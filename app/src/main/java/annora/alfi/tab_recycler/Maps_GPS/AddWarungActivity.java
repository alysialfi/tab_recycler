package annora.alfi.tab_recycler.Maps_GPS;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import annora.alfi.tab_recycler.R;
import annora.alfi.tab_recycler.Realm.RealmHelper;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class AddWarungActivity extends AppCompatActivity {
    public static final int REQUEST_IMAGE = 100;
    public static final int REQUEST_PERMISSION = 200;
    EditText edt_nama, edt_latitude, edt_longitude, edt_namaFoto, edt_alamat;
    Button button, button2;
    Realm realm;
    RealmHelper realmHelper;
    ImageView img1;
    private String imageFilePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_warung_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Point");

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        edt_nama = findViewById(R.id.edt_nama);
        edt_latitude = findViewById(R.id.edt_latitude);
        edt_longitude = findViewById(R.id.edt_longitude);
        edt_namaFoto = findViewById(R.id.edt_namaFoto);
        edt_alamat = findViewById(R.id.edt_alamat);

        edt_latitude.setText(String.valueOf(getIntent().getExtras().getDouble("LATITUDE")));
        edt_longitude.setText(String.valueOf(getIntent().getExtras().getDouble("LONGITUDE")));

        button = findViewById(R.id.button);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Warung warung = new Warung();
                    warung.setNama(edt_nama.getText().toString());
                    warung.setLatitude(Double.parseDouble(edt_latitude.getText().toString()));
                    warung.setLongitude(Double.parseDouble(edt_longitude.getText().toString()));
                    warung.setNama_image(edt_namaFoto.getText().toString());
                    warung.setAlamat(edt_alamat.getText().toString());
                    warung.setImage(imageFilePath);

                    realmHelper = new RealmHelper(realm);
                    realmHelper.save_warung(warung);
                    Toast.makeText(getApplicationContext(), "Point Added", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    //Toast.makeText(getApplicationContext(), "Meal already added before", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button2 = findViewById(R.id.button2);
        img1 = findViewById(R.id.tampilan);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(AddWarungActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddWarungActivity.this, new String[]{
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_PERMISSION);
                } else {
                    openCameraIntent();
                }
            }

        });

    }

    private void openCameraIntent() {
        Intent pctIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pctIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;

            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            Uri photoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", photoFile);
            pctIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(pctIntent, REQUEST_IMAGE);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thanks for granting me", Toast.LENGTH_SHORT).show();
                openCameraIntent();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                img1.setImageURI(Uri.parse(imageFilePath));
                Log.d("File Path", imageFilePath);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "You cancelled me", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File createImageFile() throws IOException {

        String folder = Environment.getExternalStorageDirectory() + "/" + "Alfi";
        File dir = new File(folder);
        if (!dir.exists()) {
            dir.mkdir();
        }

        File folderku = new File(Environment.getExternalStorageDirectory() + "/AlfiAnnora");
        boolean success = true;
        if (!folderku.exists()) {
            success = folderku.mkdirs();
        }
        if (success) {
            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imgFileName = edt_namaFoto.getText().toString() + "_" + timeStamp;
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = dir;
        File image = File.createTempFile(imgFileName, ".jpg", folderku);
        imageFilePath = image.getAbsolutePath();

        return image;
    }
}
