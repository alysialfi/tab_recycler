package annora.alfi.tab_recycler.Maps_GPS;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import annora.alfi.tab_recycler.R;
import annora.alfi.tab_recycler.Realm.RealmHelper;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab3_Fragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    Marker m;
    Realm realm;
    RealmHelper realmHelper;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private Double global_latitude, global_longitude;
    private Boolean init = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tab3, container, false);

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        //GPS
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
        }

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, this);
        //GPS

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Button btnChangeMapHybrid = v.findViewById(R.id.btn2);
        btnChangeMapHybrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                //mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(51.5, -0.1), new LatLng(40.7, -74.0))
                        .width(5)
                        .color(Color.BLUE));
            }
        });


        Button btnChangeMap = v.findViewById(R.id.btn1);
        btnChangeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });


        Button btnAddWarung = v.findViewById(R.id.btn3);
        btnAddWarung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addWarungActivity = new Intent(getContext(), AddWarungActivity.class);
                addWarungActivity.putExtra("LATITUDE", global_latitude);
                addWarungActivity.putExtra("LONGITUDE", global_longitude);
                startActivity(addWarungActivity);

            }
        });

        return v;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_FINE_LOCATION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                if (ActivityCompat.checkSelfPermission(
                        getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, this);
            }
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(getContext());
        mMap.setInfoWindowAdapter(customInfoWindow);
        load_point();

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                m.remove();

                Intent addWarungActivity = new Intent(getContext(), AddWarungActivity.class);
                addWarungActivity.putExtra("LATITUDE", latLng.latitude);
                addWarungActivity.putExtra("LONGITUDE", latLng.longitude);
                startActivity(addWarungActivity);

            }
        });

        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setTrafficEnabled(true);
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        mUiSettings.setRotateGesturesEnabled(true);

        MarkerOptions marker = new MarkerOptions().title("init").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).position(new LatLng(-34, 151));
        m = mMap.addMarker(marker);
    }

    @Override
    public void onLocationChanged(Location location) {
        m.remove();

        String str = "Latitude: " + location.getLatitude() + "Longitude: " + location.getLongitude();
        global_latitude = location.getLatitude();
        global_longitude = location.getLongitude();


        InfoWindowData info = new InfoWindowData();
        info.setImage("ic_action_name");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");


        LatLng area = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions marker = new MarkerOptions().title("We are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).position(area);
        m = mMap.addMarker(marker);
        m.setTag(info);

        if (init) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(area));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14.0f));
            init = false;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onResume() {
        if (!init) {
            load_point();
        }
        super.onResume();
    }

    private void load_point() {
        List<Warung> warungs = realmHelper.getAllWarung();
        for (Warung warung : warungs) {
            InfoWindowData info = new InfoWindowData();
            info.setImage(warung.getImage());
            info.setFood(warung.getNama());
            info.setTransport(warung.getAlamat());
            info.setHotel(String.valueOf(warung.getLatitude() + ", " + String.valueOf(warung.getLongitude())));

            MarkerOptions marker_warung = new MarkerOptions().
                    title(warung.getNama()).
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).
                    position(new LatLng(warung.getLatitude(), warung.getLongitude()));
            Marker mw = mMap.addMarker(marker_warung);
            mw.setTag(info);
            mw.showInfoWindow();

            mMap.setInfoWindowAdapter(new CustomInfoWindowGoogleMap(getContext()));
        }
    }
}