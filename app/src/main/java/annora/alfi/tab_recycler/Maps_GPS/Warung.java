package annora.alfi.tab_recycler.Maps_GPS;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Warung extends RealmObject {
    @PrimaryKey
    private int id;
    private String image;
    private String nama_image;
    private String nama;
    private String alamat;
    private Double latitude;
    private Double longitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNama_image() {
        return nama_image;
    }

    public void setNama_image(String nama_image) {
        this.nama_image = nama_image;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
