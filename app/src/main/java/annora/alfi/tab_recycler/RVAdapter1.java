package annora.alfi.tab_recycler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import annora.alfi.tab_recycler.API.Items;
import annora.alfi.tab_recycler.Realm.FavoriteModel;
import annora.alfi.tab_recycler.Realm.RealmHelper;
import io.realm.Realm;

public class RVAdapter1 extends RecyclerView.Adapter<RVAdapter1.ViewHolder> {

    List<Items.mealList> listItemRV1s;
    Context context;
    Realm realm;
    RealmHelper realmHelper;
    FavoriteModel favoriteModel;

    public RVAdapter1(List<Items.mealList> listItemRV1s, Context context) {
        this.listItemRV1s = listItemRV1s;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listitem_1, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final Items.mealList listItemRV1 = listItemRV1s.get(i);

        //viewHolder.imageView.setImageResource(listItemRV1.getImageView());
        viewHolder.judul.setText(listItemRV1.getStrMeal());
        viewHolder.desc.setText(listItemRV1.getStrCategory());
        Picasso.get().load(listItemRV1.getStrMT()).into(viewHolder.imageView);

        // Setup Realm
//        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
//        realm = Realm.getInstance(configuration);
//        realmHelper = new RealmHelper(realm);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                favoriteModel = new FavoriteModel();
//                favoriteModel.setNamaMkn(listItemRV1.getStrMeal());
//                favoriteModel.setKategori(listItemRV1.getStrCategory());
//                favoriteModel.setUrlFoto(listItemRV1.getStrMT());
//
//                realmHelper = new RealmHelper(realm);
//                realmHelper.save(favoriteModel);
//
//                Toast.makeText(context, "data tambah", Toast.LENGTH_SHORT).show();

                openDtl(listItemRV1s.get(i).getStrMeal(), listItemRV1s.get(i).getStrCategory(), listItemRV1s.get(i).getStrMT());

            }
        });
    }

    private void openDtl(String title, String category, String MT) {
        Intent dtl = new Intent(context, DetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("Title", title);
        bundle.putString("Category", category);
        bundle.putString("MT", MT);
        bundle.putInt("Id", 1);
        dtl.putExtras(bundle);
        context.startActivity(dtl);
    }

    @Override
    public int getItemCount() {
        return listItemRV1s.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView judul;
        private TextView desc;
        private int posisi;
        private LinearLayout linearLayout;
        //private LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image1);
            judul = itemView.findViewById(R.id.tv1);
            desc = itemView.findViewById(R.id.tv2);
            linearLayout = itemView.findViewById(R.id.linearlayout1);
            //linearLayout = itemView.findViewById(R.id.linearlayout1);
        }
    }
}
