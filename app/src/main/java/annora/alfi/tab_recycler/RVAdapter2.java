package annora.alfi.tab_recycler;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import annora.alfi.tab_recycler.API.Items2;
import annora.alfi.tab_recycler.Maps_GPS.Warung;

public class RVAdapter2 extends RecyclerView.Adapter<RVAdapter2.ViewHolder> {

    private List<Items2.catPict> lists;
    private Context context;
    private List<Warung> warungs;

    public RVAdapter2(ArrayList<Items2.catPict> lists, Context context, List<Warung> warungs) {
        this.lists = lists;
        this.context = context;
        this.warungs = warungs;
    }

    @NonNull
    @Override
    public RVAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listitem_2, viewGroup, false);

        return new RVAdapter2.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapter2.ViewHolder viewHolder, int i) {
//        final Items2.catPict catPict = lists.get(i);
        Warung warung = warungs.get(i);

        viewHolder.imageView.setImageURI(Uri.parse(warung.getImage()));

        //viewHolder.imageView.setImageResource(mealList.);
//        Picasso.get().load(catPict.getSct()).into(viewHolder.imageView);

//        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, catPict.getSct(), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return warungs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image1);
        }
    }
}
