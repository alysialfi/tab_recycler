package annora.alfi.tab_recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import annora.alfi.tab_recycler.Realm.FavoriteModel;

public class RVAdapter2_2 extends RecyclerView.Adapter<RVAdapter2_2.ViewHolder> {

    List<FavoriteModel> listItemRV2_2s;
    Context context;

    public RVAdapter2_2(List<FavoriteModel> listItemRV2_2s, Context context) {
        this.listItemRV2_2s = listItemRV2_2s;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listitem_2_2, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        FavoriteModel listItemRV2_2 = listItemRV2_2s.get(i);

        Picasso.get().load(listItemRV2_2.getUrlFoto()).into(viewHolder.imageView);
        viewHolder.textView.setText(listItemRV2_2.getNamaMkn());
        viewHolder.textView2.setText(listItemRV2_2.getKategori());
    }

    @Override
    public int getItemCount() {
        return listItemRV2_2s.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;
        TextView textView2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image1);
            textView = itemView.findViewById(R.id.tv1);
            textView2 = itemView.findViewById(R.id.tv2);
        }
    }
}
