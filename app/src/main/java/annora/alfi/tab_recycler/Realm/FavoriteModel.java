package annora.alfi.tab_recycler.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FavoriteModel extends RealmObject {
    String namaMkn, kategori, urlFoto;

    @PrimaryKey
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamaMkn() {
        return namaMkn;
    }

    public void setNamaMkn(String namaMkn) {
        this.namaMkn = namaMkn;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}
