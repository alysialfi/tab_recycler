package annora.alfi.tab_recycler.Realm;

import android.util.Log;

import java.util.List;

import annora.alfi.tab_recycler.Maps_GPS.Warung;
import io.realm.Realm;
import io.realm.RealmResults;

public class RealmHelper {
    Realm realm;

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    // untuk menyimpan data
    public void save(final FavoriteModel favoriteModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "DB was created");
                    Number currentIdNum = realm.where(FavoriteModel.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    favoriteModel.setId(nextId);
                    FavoriteModel model = realm.copyToRealm(favoriteModel);
                } else {
                    Log.e("", "DB not Found");
                }
            }
        });
    }

    // untuk memanggil semua data
    public List<FavoriteModel> getFavorite() {
        RealmResults<FavoriteModel> results = realm.where(FavoriteModel.class).findAll();
        return results;
    }

    // untuk meng-update data
    public void update(final Integer id, final String nama, final String kategori, final String foto) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                FavoriteModel model = realm.where(FavoriteModel.class)
                        .equalTo("id", id)
                        .findFirst();
                model.setNamaMkn(nama);
                model.setKategori(kategori);
                model.setUrlFoto(foto);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e("", "onSuccess: Update Successfully");
            }
        }, new Realm.Transaction.OnError() {

            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });
    }


    // untuk menghapus data
    public void delete(Integer id) {
        final RealmResults<FavoriteModel> model = realm.where(FavoriteModel.class).equalTo("id", id).findAll();
        model.size();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                model.deleteFromRealm(0);
            }
        });
    }

    //simpan data warung
    public void save_warung(final Warung warung) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "DB was created");
                    Number currentIdNum = realm.where(Warung.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    warung.setId(nextId);
                    realm.copyToRealm(warung);
                } else {
                    Log.e("", "DB not Found");
                }
            }
        });
    }

    //tampil warung
    public List<Warung> getAllWarung() {
        RealmResults<Warung> results = realm.where(Warung.class).findAll();
        return results;
    }

}
