package annora.alfi.tab_recycler.SharedPreferences;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import annora.alfi.tab_recycler.R;

public class SaveEditData extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.save_edit);

        final LinearLayout ll1 = findViewById(R.id.ll_save);
        final EditText save_nama = findViewById(R.id.etNama);
        final EditText save_email = findViewById(R.id.etEmail);
        final EditText save_alamat = findViewById(R.id.etAlamat);
        Button btn1 = findViewById(R.id.btnSave);

        final LinearLayout ll2 = findViewById(R.id.ll_edit);
        final TextView edit_nama = findViewById(R.id.tvNama);
        final TextView edit_email = findViewById(R.id.tvEmail);
        final TextView edit_alamat = findViewById(R.id.tvAlamat);
        Button btn2 = findViewById(R.id.btnEdit);

        String nama = Preferences.getNama(getApplicationContext());
        save_nama.setText(nama);
        String email = Preferences.getEmail(getApplicationContext());
        save_email.setText(email);
        String alamat = Preferences.getAlamat(getApplicationContext());
        save_alamat.setText(alamat);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setNama(getApplicationContext(), save_nama.getText().toString());
                if (!save_nama.getText().toString().equals("")) {
                    save_nama.setText("");
                }
                Preferences.setEmail(getApplicationContext(), save_email.getText().toString());
                if (!save_email.getText().toString().equals("")) {
                    save_email.setText("");
                }

                Preferences.setAlamat(getApplicationContext(), save_alamat.getText().toString());
                if (!save_alamat.getText().toString().equals("")) {
                    save_alamat.setText("");
                }

                ll1.setVisibility(View.GONE);
                ll2.setVisibility(View.VISIBLE);

                String nama = Preferences.getNama(getApplicationContext());
                edit_nama.setText(nama);
                String email = Preferences.getEmail(getApplicationContext());
                edit_email.setText(email);
                String alamat = Preferences.getAlamat(getApplicationContext());
                edit_alamat.setText(alamat);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll2.setVisibility(View.GONE);
                ll1.setVisibility(View.VISIBLE);

                String nama = Preferences.getNama(getApplicationContext());
                save_nama.setText(nama);
                String email = Preferences.getEmail(getApplicationContext());
                save_email.setText(email);
                String alamat = Preferences.getAlamat(getApplicationContext());
                save_alamat.setText(alamat);

            }
        });
    }
}
