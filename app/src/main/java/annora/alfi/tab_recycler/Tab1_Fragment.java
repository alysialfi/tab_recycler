package annora.alfi.tab_recycler;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import annora.alfi.tab_recycler.API.ApiClient;
import annora.alfi.tab_recycler.API.ApiInterface;
import annora.alfi.tab_recycler.API.Items;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class Tab1_Fragment extends Fragment {

    Context context;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListItemRV1> listItemRV1s;

    public Tab1_Fragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tab1, container, false);

        recyclerView = v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        listItemRV1s = new ArrayList<>();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Items> call = apiService.getData();

        call.enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {
                ArrayList<Items.mealList> mealLists = response.body().getMeals();
                adapter = new RVAdapter1(mealLists, getContext());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {

            }
        });

        return v;

    }
}
