package annora.alfi.tab_recycler;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import annora.alfi.tab_recycler.API.ApiClient;
import annora.alfi.tab_recycler.API.ApiInterface;
import annora.alfi.tab_recycler.API.Items2;
import annora.alfi.tab_recycler.Maps_GPS.Warung;
import annora.alfi.tab_recycler.Realm.FavoriteModel;
import annora.alfi.tab_recycler.Realm.RealmHelper;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tab2_Fragment extends Fragment {

    Realm realm;
    RealmHelper realmHelper;
    private RecyclerView recyclerView;
    private RecyclerView recyclerView2;
    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter adapter2;
    private List<ListItemRV2> listItemRV2s;
    SwipeRefreshLayout swr1;
    private List<FavoriteModel> listItemRV2_2s;


    public Tab2_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tab2, container, false);

        // Setup Realm
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        recyclerView = v.findViewById(R.id.recyclerView);
        recyclerView2 = v.findViewById(R.id.recyclerView2);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView2.setLayoutManager(linearLayoutManager2);

        listItemRV2s = new ArrayList<>();
        listItemRV2_2s = new ArrayList<>();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Items2> call = apiService.getData2();

        call.enqueue(new Callback<Items2>() {
            @Override
            public void onResponse(Call<Items2> call, Response<Items2> response) {
                ArrayList<Items2.catPict> catPicts = response.body().getCatPicts();
                List<Warung> warungs = realmHelper.getAllWarung();
                adapter = new RVAdapter2(catPicts, getContext(), warungs);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Items2> call, Throwable t) {

            }
        });


        listItemRV2_2s = realmHelper.getFavorite();
        adapter2 = new RVAdapter2_2(listItemRV2_2s, getActivity());
        recyclerView2.setAdapter(adapter2);

//        swr1.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//
//
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        swr1.setRefreshing(false);
//
//                        listItemRV2_2s= realmHelper.getFavorite();
//                        adapter2 = new RVAdapter2_2(listItemRV2_2s, getActivity());
//                        recyclerView2.setAdapter(adapter2);
//                    }
//                }, 2000);
//            }
//
//        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        listItemRV2_2s = realmHelper.getFavorite();
        adapter2 = new RVAdapter2_2(listItemRV2_2s, getActivity());
        recyclerView2.setAdapter(adapter2);
    }
}
